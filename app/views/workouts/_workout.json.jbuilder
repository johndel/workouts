json.extract! workout, :id, :workout_date, :comments, :weight, :bone_density, :bmi, :body_fat, :muscle_mass, :water, :created_at, :updated_at
json.url workout_url(workout, format: :json)
