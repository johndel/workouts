class Workout < ApplicationRecord
  belongs_to :member
  has_many :exercises

  accepts_nested_attributes_for :exercises, allow_destroy: true, reject_if: :all_blank
end
