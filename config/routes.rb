Rails.application.routes.draw do
  resources :exercises
  resources :workouts
  resources :members
  root 'members#index', as: :authenticated_root
  devise_for :users
end
