json.extract! exercise, :id, :name, :total_set, :weights, :duration, :comments, :workout_id, :created_at, :updated_at
json.url exercise_url(exercise, format: :json)
