class CreateWorkouts < ActiveRecord::Migration[5.2]
  def change
    create_table :workouts do |t|
      t.datetime :workout_date
      t.text :comments
      t.decimal :weight
      t.decimal :bone_density
      t.decimal :bmi
      t.decimal :body_fat
      t.decimal :muscle_mass
      t.decimal :water
      t.integer :member_id

      t.timestamps
    end
    add_index :workouts, :member_id
  end
end
