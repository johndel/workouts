class CreateExercises < ActiveRecord::Migration[5.2]
  def change
    create_table :exercises do |t|
      t.string :name
      t.integer :total_set
      t.decimal :weights
      t.integer :duration
      t.text :comments
      t.integer :workout_id

      t.timestamps
    end
    add_index :exercises, :workout_id
  end
end
